package sdv.devices.arduino;

import java.util.Scanner;
import arduino.*;

/**
 * Sets up a connection to an Arduino connected to the system.
 * Writes to the Arduino.
 *
 * @author Ole-martin Steinnes
 * @version 0.1
 */
public class ArduinoComm {

    private Scanner scanner;
    private Arduino arduino;
    private boolean finished = false;

    public ArduinoComm() {
        setUpComm();
        setUpArduino();
    }


    public void run() {
        dummyCommLogic();
    }

    private void setUpComm() {
        scanner = new Scanner(System.in);
        arduino = new Arduino();
    }

    private void setUpArduino() {
        arduino.openConnection();
    }

    private void dummyCommLogic() {
        char userInput = scanner.nextLine().charAt(0);
        while (!finished) {
            if (userInput == 'q'){
                userInput = scanner.nextLine().charAt(0);
                finished = true;
            } else if (userInput == '1') {
                arduino.serialWrite(userInput);
                userInput = scanner.nextLine().charAt(0);
            } else if (userInput == '2') {
                arduino.serialWrite(userInput);
                userInput = scanner.nextLine().charAt(0);
            } else {
                userInput = scanner.nextLine().charAt(0);
                arduino.closeConnection();
                finished = true;
            }
        }
    }
}
