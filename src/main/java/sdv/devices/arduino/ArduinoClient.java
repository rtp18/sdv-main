package sdv.devices.arduino;

/**
 * @author Ole-martin Steinnes
 * @version 0.1
 */
public class ArduinoClient {

    private ArduinoComm arduinoComm;


    public ArduinoClient() {

        arduinoComm = new ArduinoComm();

    }

    public void runArduinoCom() {
        arduinoComm.run();
    }
}
