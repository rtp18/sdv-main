package sdv.devices.arduino;

import sdv.sensors.imu.Imu;

/**
 * @author Ole-martin Steinnes
 */
public class ArduinoImuClient {

    private Imu imu;

    public ArduinoImuClient(Imu imu) {
        this.imu = imu;
    }
}
